﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

interface IView
{
    void Show();
    void Hide();
    void Back();
}
