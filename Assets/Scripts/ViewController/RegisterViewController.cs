﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterViewController : MonoBehaviour {

    private GameObject Panel;
    private RegisterPanelScript panelScript;

    private bool isHidden = true;
    public bool IsHidden { get { return isHidden; } }

    //private Animator animator;
    //private int triggerId;

    public void Back()
    {
        Hide();
    }

    public void Hide()
    {
        isHidden = true;
        panelScript.ShowView();
        //animator.SetTrigger(triggerId);
    }

    public void Show()
    {
        isHidden = false;
        panelScript.ShowView();
        //animator.SetTrigger(triggerId);
    }

    // Use this for initialization
    void Start()
    {
        Panel = this.gameObject;
        panelScript = Panel.GetComponent<RegisterPanelScript>();
        //animator = Panel.GetComponent<Animator>();
        //triggerId = Animator.StringToHash(TriggerId);
    }
}
