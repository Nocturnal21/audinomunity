﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideViewController : MonoBehaviour {

    private GameObject Panel;
    public string TriggerId;

    private bool isHidden = true;
    public bool IsHidden { get { return isHidden; } }

    private Animator animator;
    private int triggerId;

    public void Back()
    {
        Hide();
    }

    public void Hide()
    {
        isHidden = true;
        animator.SetTrigger(triggerId);
    }

    public void Show()
    {
        isHidden = false;
        animator.SetTrigger(triggerId);
    }

    // Use this for initialization
    void Start()
    {
        Panel = this.gameObject;
        animator = Panel.GetComponent<Animator>();
        triggerId = Animator.StringToHash(TriggerId);
    }
}
