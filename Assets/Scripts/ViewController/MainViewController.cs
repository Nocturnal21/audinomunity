﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainViewController : MonoBehaviour, IView
{

    // Set by Unity Editor
    private GameObject Panel;

    private bool isHidden = false;
    public bool IsHidden { get { return isHidden; } }

    public Animator animatorSidePanel, animatorRegisterPanel;
    public string TriggerId;
    private int triggerId;

    public void Back()
    {
        Application.Quit();
    }

    public void Hide()
    {
        this.Panel.SetActive(false);
    }

    public void Show()
    {
        this.Panel.SetActive(true);
    }

    public void HideBars(Message m)
    {
        var set = m.BoolValue;
        animatorSidePanel.SetBool(triggerId, set);
        animatorRegisterPanel.SetBool(triggerId, set);
    }

    // Use this for initialization
    void Start()
    {
        Panel = this.gameObject;

        triggerId = Animator.StringToHash(TriggerId);
        EventManager.Instance.StartListening("HideBars", HideBars);
    }
}
