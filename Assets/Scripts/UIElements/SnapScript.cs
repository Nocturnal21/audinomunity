﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class SnapScript : MonoBehaviour {

    public RectTransform content, picker;
    public GameObject elementPrefab;
    public int lowerBound, upperBound;
    public int width, height;

    private ScrollRect scrollRect;
    private List<Text> elements;

    private int numberOfElements;
    private float step;

	// Use this for initialization
	void Start () {
        scrollRect = GetComponent<ScrollRect>();
        picker = GetComponent<RectTransform>();

        numberOfElements = upperBound - lowerBound + 1;
        content.sizeDelta = new Vector2(width, numberOfElements * height);
        picker.sizeDelta = new Vector2(width, height * 2);

        var layout = content.GetComponent<VerticalLayoutGroup>();
        layout.padding = new RectOffset(0, 0, height / 2, height / 2);

        step = 1.0f / (numberOfElements - 1);
        scrollRect.verticalNormalizedPosition = 0 - step / 2;

        for(var i = lowerBound; i < upperBound+1; ++i)
        {
            var go = Instantiate(elementPrefab, content.transform, false);
            go.GetComponent<Text>().text = i.ToString();
        }
	}

    public void OnScrolling(Vector2 value)
    {
        if(scrollRect.verticalNormalizedPosition < 0.0f)
        {
            scrollRect.verticalNormalizedPosition = 0;
            scrollRect.velocity = Vector2.zero;
        }
        else if(scrollRect.verticalNormalizedPosition > 1.0f)
        {
            scrollRect.verticalNormalizedPosition = 1;
            scrollRect.velocity = Vector2.zero;
        } else if(Mathf.Abs(scrollRect.velocity.y) < 80.0f)
        {
            scrollRect.velocity = Vector2.zero;
            float over, under;
            for (over = 0; over < scrollRect.verticalNormalizedPosition; over += step) ;
            under = over - step;

            if (scrollRect.verticalNormalizedPosition - under < over - scrollRect.verticalNormalizedPosition)
                scrollRect.verticalNormalizedPosition = under;
            else
                scrollRect.verticalNormalizedPosition = over;

            var index = GetCurrentIndex();
            Debug.Log(this.gameObject.name + " Value: " + index);
            EventManager.TriggerEvent(this.gameObject.name + "ValueChanged", new Message(index));
        }
    }

    public int GetCurrentIndex()
    {
        var index = (int)(upperBound - scrollRect.verticalNormalizedPosition * numberOfElements + 1);
        if (index > upperBound)
            index = upperBound;

        return index;
    }
}
