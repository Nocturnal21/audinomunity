﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableFree : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("DragBegin");
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = new Vector3( eventData.position.x, this.transform.position.y);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("DragEnd");
    }
}
