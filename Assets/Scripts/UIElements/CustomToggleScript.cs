﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomToggleScript : MonoBehaviour {

    public Color toggleOffColor;
    public Color toggleOnColor;
    public string eventName;
    public string presetName;

    public Image background;
    private bool isOn = false;
	// Use this for initialization
	void Start () {
        isOn = GetComponent<Toggle>().isOn;
        GetComponentInChildren<Text>().text = presetName;

        switch(isOn)
        {
            case true:  background.color = toggleOnColor; break;
            case false: background.color = toggleOffColor; break;
        }
	}

    public void Toggle(bool value)
    {
        isOn = value;
        if(isOn)
        {
            background.color = toggleOnColor;
            EventManager.TriggerEvent(eventName, new Message(presetName));
        }
        else
        {
            background.color = toggleOffColor;
        }
    }
}
