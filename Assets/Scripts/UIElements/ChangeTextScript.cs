﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeTextScript : MonoBehaviour {

    public string disabledText;
    public string enabledText;

    private bool isEnabled;
    private Text text;
    // Use this for initialization
    public void Start()
    {
        this.text = GetComponent<Text>();
        isEnabled = false;
    }

    public void ChangeText()
    {
        isEnabled = !isEnabled;
        if (isEnabled)
            this.text.text = enabledText;
        else
            this.text.text = disabledText;
    }
}
