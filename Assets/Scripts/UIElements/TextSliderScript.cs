﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSliderScript : MonoBehaviour {

    public string Name;
    public string Description;
    public int Value;
    public int Min, Max;

    private Text currentStringValue;

	// Use this for initialization
	void Start () {
        this.transform.Find("TextPanel").Find("Description").gameObject.GetComponent<Text>().text = Description;
        currentStringValue = this.transform.Find("TextPanel").Find("StringValue").gameObject.GetComponent<Text>();
        currentStringValue.text = Value.ToString();

        var slider = this.transform.Find("Slider").gameObject.GetComponent<Slider>();
        slider.value = Value;
        slider.minValue = Min;
        slider.maxValue = Max;
    }

    public void ValueChanged(float value)
    {
        Value = (int)value;
        EventManager.TriggerEvent(Name + "Slider", new Message(Value));
        currentStringValue.text = value.ToString();
    }
}
