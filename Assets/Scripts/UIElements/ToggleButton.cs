﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ToggleButton : Toggle
{
    #region Inspector
    // ReSharper disable InconsistentNaming
    Sprite normalSprite;
    // ReSharper restore InconsistentNaming
    #endregion

    public delegate void NoteClicked(string name, bool state);
    public event NoteClicked OnClicked;

    protected override void Start()
    {
        base.Start();

        normalSprite = ((Image)targetGraphic).sprite;
        onValueChanged.AddListener(value =>
        {
            switch (transition)
            {
                case Transition.ColorTint: image.color = isOn ? colors.pressedColor : colors.normalColor; break;
                case Transition.SpriteSwap: image.sprite = isOn ? spriteState.pressedSprite : normalSprite; break;
                case Transition.Animation: break;
                default: throw new NotImplementedException();
            }

            raiseEvent(value);
        });
    }

    void raiseEvent(bool state)
    {
        if (OnClicked != null)
            OnClicked(name, state);
    }
}