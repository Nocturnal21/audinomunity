﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableX : MonoBehaviour, IDragHandler {

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = new Vector3( eventData.position.x, this.transform.position.y);
    }
}
