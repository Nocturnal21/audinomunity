﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler {

    private RectTransform t;
    private Vector2 position;

    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnDrag(PointerEventData eventData)
    {
        t.localPosition = t.anchoredPosition + new Vector2(eventData.delta.x, 0);
        Debug.Log("x: " + t.localPosition.x);
    }

    public void OnDrop(PointerEventData eventData)
    {
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }

    // Use this for initialization
    void Start () {
        t = this.gameObject.GetComponent<RectTransform>();
        position = t.anchoredPosition;
	}
	
}
