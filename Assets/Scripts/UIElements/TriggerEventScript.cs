﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEventScript : MonoBehaviour {
    public string eventName;

	public void TriggerEvent()
    {
        Debug.Log("Triggered Event: " + eventName);
        EventManager.TriggerEvent(eventName, null);
    }

    public void TriggerBoolEvent(bool value)
    {
        EventManager.TriggerEvent(eventName, new Message(value));
    }
}
