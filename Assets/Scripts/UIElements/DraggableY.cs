﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableY : MonoBehaviour, IDragHandler {

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = new Vector3( this.transform.position.x, eventData.position.y);
    }

}
