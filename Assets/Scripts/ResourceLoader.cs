﻿using System;
using UnityEngine;


public class ResourceLoader
{

    public static AudioClip[] LoadSoundPack(string name, string octave1, string octave2, string octave3)
    {
        var sounds = new AudioClip[36];
        string currentOctave;
        string postFix;
        int offset = 0;
        for (var iOct = 0; iOct < 3; ++iOct)
        {
            switch (iOct)
            {
                case 0: currentOctave = octave1; postFix = "_1"; break;
                case 1: currentOctave = octave2; postFix = "_2"; break;
                case 2: currentOctave = octave3; postFix = "_3"; break;
                default: currentOctave = postFix = ""; break;
            }
            for (int i = 1; i < 13; ++i)
            {
                string index = (i < 10) ? "0" + i : "" + i;
                string path = name + "/" + name + " " + currentOctave + "/" + index + postFix;
                sounds[offset + i - 1] = Resources.Load(path) as AudioClip;

                if (sounds[offset + i - 1] == null)
                {
                    Debug.LogError("Couldn't load Resource: " + path);
                    return null;
                }
            }
            offset += 12;
        }
        return sounds;
    }

    public static AudioClip[] LoadSoundPackFragment(string name, string octave1, string octave2, string octave3) {
        var sounds = new AudioClip[9]; // contains Cis, F, A, cis, f, a, cis', f', a' where a's are @index 2,5,8
        string currentOctave;
        string postFix;
        int offset = 0;

        for (var iOct = 0; iOct < 3; ++iOct)
        {
            switch (iOct)
            {
                case 0: currentOctave = octave1; postFix = "_1"; break;
                case 1: currentOctave = octave2; postFix = "_2"; break;
                case 2: currentOctave = octave3; postFix = "_3"; break;
                default: currentOctave = postFix = ""; break;
            }

            var iI = 2;
            for (var i = 1; i < 4; ++i)
            {
                string prefix = (iI < 10) ? "0" : "";
                string path = name + "/" + name + " " + currentOctave + "/" + prefix + iI + postFix;
                sounds[offset + i - 1] = Resources.Load(path) as AudioClip;

                if (sounds[offset + i - 1] == null)
                {
                    Debug.LogError("Couldn't load Resource: " + path);
                    return null;
                }

                iI += 4;
            }
            offset += 3;
        }
        return sounds;
    }
}
