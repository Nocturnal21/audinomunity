﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System;
using MicroLibrary;

public class TimerScript : MonoBehaviour
{

    //public delegate void TickHandler();
    //public static event TickHandler Tick;

    public delegate void StopHandler();
    public static event StopHandler Stop;

    private int bpm;
    private float interval; // Time interval between beats
    private bool running;

    public AudioPlayerScript player;

    // Time
    private MicroTimer mTimer;

    // Use this for initialization
    void Start()
    {
        EventManager.Instance.StartListening("BPMSlider", ChangeBPM);

        bpm = 90;
        interval = 60.0f / bpm;

        mTimer = new MicroTimer((long)(interval * 1000000));
        mTimer.MicroTimerElapsed += TickFunction;
        mTimer.Start();

        running = true;
    }

    public void Play()
    {
        if (!running)
        {
            mTimer = new MicroTimer((long)(interval * 1000000));
            mTimer.MicroTimerElapsed += TickFunction;
            mTimer.Start();

        }
        else
        {
            mTimer.Stop();
            Stop();
        }
    }

    void TickFunction(object sender, MicroTimerEventArgs args)
    {
        Console.WriteLine("Timer: " + args.ElapsedMicroseconds);
        player.Invoke("Tick", 0);
    }

    public void ChangeBPM(Message m)
    {
        this.bpm = m.IntValue;
        this.interval = 60.0f / this.bpm;

        mTimer.Stop();
        Stop();

        mTimer = new MicroTimer((long)(interval * 1000000));
        mTimer.MicroTimerElapsed += TickFunction;
        mTimer.Start();
    }
}
