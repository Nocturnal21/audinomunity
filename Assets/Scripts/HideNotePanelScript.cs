﻿using UnityEngine;
using System.Collections;

public class HideNotePanelScript : MonoBehaviour {

    public GameObject panel_notes;

    public void HideToggle(bool hide)
    {
        if (hide)
            panel_notes.SetActive(false);
        else
            panel_notes.SetActive(true);
    }
}
