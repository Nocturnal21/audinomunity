﻿using UnityEngine;
using System.Collections;

public class UIManagerScript : MonoBehaviour {
    private AudioPlayerScript audio_player;
    private AudioSource source;
    //public TimerScript timer;
    public GameObject panel_notes;
    public GameObject noteInputField;
    public GameObject fileInputField;

    void Start()
    {
        // To prevent app from falling asleep...
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.runInBackground = true;
    }

    //public void PlayButtonClicked()
    //{
    //    timer.Play();
    //}

//    void Update()
//    {
//#if UNITY_EDITOR || UNITY_STANDALONE
//        // Start to play by clicking space
//        if (Input.GetKeyDown(KeyCode.Space) &&
//            noteInputField.GetComponent<UnityEngine.UI.InputField>().isFocused == false &&
//            fileInputField.GetComponent<UnityEngine.UI.InputField>().isFocused == false)
//        {
//            throw new System.NotImplementedException();
//        }
//#endif
//    }
}