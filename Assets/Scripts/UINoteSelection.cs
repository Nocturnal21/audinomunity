﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UINoteSelection : MonoBehaviour {
    public enum BTYPE : byte { bpm_type = 0, bpn_type, duration_type };
    public enum OCTAVE : byte { low = 1, mid, high };

    public ToggleButton[] buttons;
    private NoteSelection selection;

    private byte current_octave;
    public GameObject PanelSelection;
    private Animator OctaveSwitchAnimator;
    private int animatorTriggerId;

    public Color low_color, mid_color, high_color;
    public PanelDotsScript PanelDots;

    public delegate void NewSelection();
    public static event NewSelection RefreshSelection;
   

    // Use this for initialization
    void Start () {
        selection = NoteSelection.Instance;
        current_octave = (byte) OCTAVE.mid;

        EventManager.Instance.StartListening("IncreaseOctave", IncOctave);
        EventManager.Instance.StartListening("DecreaseOctave", DecOctave);

        OctaveSwitchAnimator =  PanelSelection.GetComponent<Animator>();
        animatorTriggerId = Animator.StringToHash("Octave");

        for(int i = 0; i < 12; ++i)
        {
            buttons[i].GetComponent<Image>().color = low_color;
            buttons[i].OnClicked += ModifyButtonState;
        }
        for(int i = 12; i < 24; ++i)
        {
            buttons[i].GetComponent<Image>().color = mid_color;
            buttons[i].OnClicked += ModifyButtonState;
        }
        for(int i = 24; i < 36; ++i)
        {
            buttons[i].GetComponent<Image>().color = high_color;
            buttons[i].OnClicked += ModifyButtonState;
        }
    }

    //Add/Remove Selection
    void ModifyButtonState(string note, bool state)
    {
        int noteNameValue = -1;
        try
        {
            noteNameValue = System.Int32.Parse(note);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Could not parse buttons name: " + note + " raised exception: " + e.ToString());
        }

        uint code = ((uint)current_octave << 4) + (uint)noteNameValue;
        if (state == true)
            selection.Add(code);
        else
            selection.Remove(code);

        PanelDots.ToggleDot((int)current_octave, (int)noteNameValue);

        //Tell subscribers that selection has changed
        RefreshSelection();
    }

    public void IncOctave(Message m)
    {
        if (current_octave < (byte)OCTAVE.high)
        {
            current_octave++;
            ChangeOctave();
        }
    }

    public void DecOctave(Message m)
    {
        if (current_octave > (byte)OCTAVE.low)
        {
            current_octave--;
            ChangeOctave();
        }
    }

    private void ChangeOctave()
    {
        switch (current_octave)
        {
            case (byte)OCTAVE.low:
                OctaveSwitchAnimator.SetInteger(animatorTriggerId, 1);
                break;
            case (byte)OCTAVE.mid:
                OctaveSwitchAnimator.SetInteger(animatorTriggerId, 2);
                break;
            case (byte)OCTAVE.high:
                OctaveSwitchAnimator.SetInteger(animatorTriggerId, 3);
                break;
        }
    }

    private void OnDestroy()
    {
        EventManager.StopListening("IncreaseOctave", IncOctave);
        EventManager.StopListening("DecreaseOctave", DecOctave);
    }
}
