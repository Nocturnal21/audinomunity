﻿using UnityEngine;
using System.Collections;

public class FilenameScript : MonoBehaviour {

    public delegate void FilenameHandler(string name);
    public static event FilenameHandler FilenameChanged;

    // Use this for initialization
    void Start () {
        var input = GetComponentInParent<UnityEngine.UI.InputField>();
        var v = new UnityEngine.UI.InputField.OnChangeEvent();
        v.AddListener(EndEditFilename);
        input.onValueChanged = v;
    }

    private void EndEditFilename(string name)
    {
        FilenameChanged(name);
    }
}
