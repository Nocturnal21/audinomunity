﻿using UnityEngine;
using System.Collections;

public class HideScript : MonoBehaviour {

    private UnityEngine.UI.Text text;

    void Start()
    {
        text = GameObject.Find("DisplayNote").GetComponent<UnityEngine.UI.Text>();
    }

    public void HideToggle(bool hide)
    {
        if (hide)
            text.enabled = false;
        else
            text.enabled = true;
    }
}
