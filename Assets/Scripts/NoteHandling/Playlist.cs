﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;


public class Playlist : MonoBehaviour, IEnumerable
{
    private static System.Random random = new System.Random();
    private NoteSelection.RandomType CurrentRandomizer = NoteSelection.RandomType.Permutation;

    private uint[] playlist;
    public uint[] PlaylistArray { get { return playlist; } }
    private uint[] buffer;
    private Thread thread; //used for calculating playlists

    public void RefreshPlaylist(NoteSelection.RandomType rtype)
    {
//        StopAllCoroutines();
        playlist = NoteSelection.Instance.Notes.ToArray();
//         buffer = (uint[])playlist.Clone();
        CurrentRandomizer = rtype;

        switch(rtype)
        {
            default: //RandomType.Permutation
                Shuffle(playlist);
                break;
            case NoteSelection.RandomType.OctaveFolding:
                RandomValuesOfOctaves(ref playlist);
                break;
        }

        FillBuffer();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    ///<summary>
    /// Returns the Playlist's enumerator. Each element is a selected note.
    ///</summary>
    public PlaylistEnumerator GetEnumerator()
    {
        return new PlaylistEnumerator(this);
    }

    ///<summary>
    /// Calculates another playlist in a separate thread and stores it in
    /// 'buffer'
    ///</summary>
    private void FillBuffer()
    {
        switch (CurrentRandomizer)
        {
            default: //RandomType.Permutation
                buffer = playlist;
                thread = new Thread(() => Shuffle(buffer));
                break;
            case NoteSelection.RandomType.OctaveFolding:
                buffer = NoteSelection.Instance.Notes.ToArray();
                thread = new Thread(() => RandomValuesOfOctaves(ref buffer));
                break;
        }
        thread.Start();
    }

    ///<summary>
    /// Switches the buffer.
    ///</summary>
    public void SwitchBuffer()
    {
//          stopallcoroutines();
        if(!thread.IsAlive) {
            playlist = buffer;
            FillBuffer();
        } else {
            Debug.LogError("[Playlist] Thread is still busy. Cannot change buffer. \n Waiting for thread to finish exectution.");
            thread.Join();
        }
    }
    
    private static void Shuffle(uint[] list)
    {
        for (int n = list.Length - 1; n > 1; n--)
        {
            int k = random.Next(n + 1);
            var value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }


    public static void RandomValuesOfOctaves(ref uint[] list)
    {
        Shuffle(list);
        var output = new List<uint>();

        foreach (var n in list)
        {
            uint noteid = 15u & n;
            if (!output.Exists(x => (15u & x) == noteid))
                output.Add(n);
        }

        list = output.ToArray();
    }

// -----> Coroutine Stuff <------
//    public IEnumerator RandomValuesOfOctavesDelayed(uint[] list)
//    {
//        yield return StartCoroutine(ShuffleDelayed(list));
//        List<uint> output = new List<uint>();
//
//        foreach (var n in list)
//        {
//            uint noteid = 15u & n;
//            if (!output.Exists(x => (15u & x) == noteid))
//                output.Add(n);
//
//            yield return 0;
//        }
//
//        buffer = output.ToArray();
//    }
//    private IEnumerator ShuffleDelayed(uint[] list)
//    {
//        for(int n = list.Length-1; n > 1; n--) {
//            int k = random.Next(n + 1);
//            var value = list[k];
//            list[k] = list[n];
//            list[n] = value;
//
//            yield return 0;
//        }
//    }
}
