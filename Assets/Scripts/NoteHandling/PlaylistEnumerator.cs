﻿using System;
using System.Collections;

public class PlaylistEnumerator : IEnumerator
{
    private int position = -1;
    private Playlist p;
    public PlaylistEnumerator(Playlist p)
    {
        this.p = p;
    }

    object IEnumerator.Current
    {
        get
        {
            return Current();
        }
    }

    public uint Current()
    {
        try
        {
            return p.PlaylistArray[position];
        }
        catch (IndexOutOfRangeException)
        {
            throw new InvalidOperationException();
        }
    }

    public bool MoveNext()
    {
        position++;
        if(position == p.PlaylistArray.Length)
        {
            p.SwitchBuffer();
            position = 0;
        }

        return (position < p.PlaylistArray.Length);
    }

    public void Reset()
    {
        position = -1;
    }
}
