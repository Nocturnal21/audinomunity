﻿using UnityEngine;

public interface IPopUp
{
    void Show();
}
