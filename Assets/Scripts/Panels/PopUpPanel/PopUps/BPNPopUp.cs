﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BPNPopUp : MonoBehaviour, IPopUp
{

    public ToggleGroup toggles;
    public Button button;

    private Dictionary<string, int> values;

    private void Start()
    {
        this.gameObject.SetActive(false);

        PopUpManager.Instance.RegisterPopUp(this.name, this);

        values = new Dictionary<string, int>();
        values.Add("Toggle-1", 1); values.Add("Toggle-2", 2); values.Add("Toggle-3", 3); values.Add("Toggle-4", 4);
        values.Add("Toggle-5", 5); values.Add("Toggle-6", 6); values.Add("Toggle-7", 7); values.Add("Toggle-8", 8);
    }

    /// <summary>
    /// Trigger this method if BPN is choosen by user.
    /// </summary>
    public void OnSelect(bool on)
    {
        if (on)
        {
            var selected = toggles.ActiveToggles().FirstOrDefault();

            int value;
            if (values.TryGetValue(selected.name, out value))
            {
                EventManager.TriggerEvent("BPNChanged", new Message(value)); // Raise Event

                button.GetComponentInChildren<Text>().text = value.ToString(); // Update Button Text
            }
            else
            {
                Debug.LogError("Could not parse Toggle value: " + selected.name);
            }

            this.gameObject.SetActive(false);
        }
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        PopUpManager.Instance.UnregisterPopUp(this.name);
    }
}
