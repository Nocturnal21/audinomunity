﻿using System.Collections.Generic;
using UnityEngine;

public class PopUpManager : MonoBehaviour {

    private Dictionary<string,IPopUp> Panels;

    private static PopUpManager instance;
    public static PopUpManager Instance
    {
        get
        {
            if (!instance)
            {
                instance = GameObject.FindObjectOfType(typeof(PopUpManager)) as PopUpManager;
                if (!instance)
                    Debug.LogError("There needs to be at least one instance of PopUpManager attached to a GameObject in the scene!");
                else
                    instance.Init();                    
            }
            return instance;
        }
    }

    private void Init() {
        if(Panels == null)
            Panels = new Dictionary<string, IPopUp>();
    }

	//// Use this for initialization
	//void Start () {
	//}
	
	//// Update is called once per frame
	//void Update () {
		
	//}

    public void ShowPopUp(string name)
    {
        IPopUp popup;
        if (Panels.TryGetValue(name, out popup))
            popup.Show();
    }

    public void RegisterPopUp(string name, IPopUp pop)
    {
        Panels.Add(name, pop);
    }

    public void UnregisterPopUp(string name)
    {
        Panels.Remove(name);
    }
}
