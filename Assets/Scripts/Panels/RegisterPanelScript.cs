﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterPanelScript : MonoBehaviour {
    public GameObject RegisterPanel1, RegisterPanel2, RegisterPanel3, RegisterPanel4;
    public GameObject RegisterButtonPanel;

    public enum RegId { Register1 = 1, Register2, Register3, Register4};
    private static RegId topRegister = RegId.Register1;
    public static RegId TopRegister { get { return topRegister; } }

    private Animator animator;
    private int triggerId;

	// Use this for initialization
	void Start () {
        animator = this.GetComponent("Animator") as UnityEngine.Animator;
        triggerId = Animator.StringToHash("AnimationTriggerRegisterPanel");

        ResetRegisterPanelOrder();
	}

    public void ShowView()
    {
        ResetRegisterPanelOrder();
        animator.SetTrigger(triggerId);
    }

    public void BringRegisterToFront(int regid)
    {
        switch((RegId) regid)
        {
            case RegId.Register1:
                RegisterPanel1.transform.SetAsLastSibling();
                RegisterButtonPanel.transform.SetAsLastSibling();
                StateManager.Instance.ChangeSubState(StateManager.UISubState.Register1);
                break;
            case RegId.Register2:
                ResetRegisterPanelOrder();
                RegisterPanel2.transform.SetAsLastSibling();
                RegisterButtonPanel.transform.SetAsLastSibling();
                StateManager.Instance.ChangeSubState(StateManager.UISubState.Register2);
                break;
            case RegId.Register3:
                ResetRegisterPanelOrder();
                RegisterPanel3.transform.SetAsLastSibling();
                RegisterButtonPanel.transform.SetAsLastSibling();
                StateManager.Instance.ChangeSubState(StateManager.UISubState.Register3);
                break;
            case RegId.Register4:
                ResetRegisterPanelOrder();
                RegisterPanel4.transform.SetAsLastSibling();
                RegisterButtonPanel.transform.SetAsLastSibling();
                StateManager.Instance.ChangeSubState(StateManager.UISubState.Register4);
                break;
        }
        topRegister = (RegId) regid;
    }

    private void ResetRegisterPanelOrder()
    {
        RegisterPanel1.transform.SetSiblingIndex(3);
        RegisterPanel2.transform.SetSiblingIndex(2);
        RegisterPanel3.transform.SetSiblingIndex(1);
        RegisterPanel4.transform.SetSiblingIndex(0);

        RegisterButtonPanel.transform.SetSiblingIndex(4);
    }
}
