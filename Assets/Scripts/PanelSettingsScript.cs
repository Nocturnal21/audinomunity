﻿using UnityEngine;
using System.Collections;

public class PanelSettingsScript : MonoBehaviour {
    public GameObject SettingsPanel;
    private Animator animator;
    private int triggerId;

    // Use this for initialization
    void Start () {
        animator = SettingsPanel.GetComponent<Animator>();
        triggerId = Animator.StringToHash("toggleSettings");
    }

    public void SettingsButtonPressed (bool value)
    {
        animator.SetTrigger(triggerId);
    }
}
