﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;

public class TextEditorManager : MonoBehaviour
{
    InputField text_editor;
    InputField inputfield_filename;
    Dropdown dropdown_files;
    Button button_save;

    private System.Collections.Generic.List<string> option_list;

    //File IO
    StreamWriter writer;
    StreamReader reader;
    string path;
    string filename;
    static private string dir = "./Notes/";
    static private string filetype = ".txt";

    //Current file
    private string current_file;

    // Use this for initialization
    void Start()
    {
        text_editor = GameObject.Find("InputFieldTextEdit").GetComponent<UnityEngine.UI.InputField>();
        dropdown_files = GameObject.Find("DropdownFiles").GetComponent<UnityEngine.UI.Dropdown>();
        button_save = GameObject.Find("ButtonSave").GetComponent<UnityEngine.UI.Button>();
        inputfield_filename = GameObject.Find("InputFieldFilename").GetComponent<UnityEngine.UI.InputField>();

        // Load note list
        option_list = new System.Collections.Generic.List<string>();
        LoadState();
        dropdown_files.AddOptions(option_list);

        Directory.CreateDirectory(dir);

        // Subscribe to filename changed event in FilenameScript
        FilenameScript.FilenameChanged += ChangeFilename;

        if (text_editor == null || dropdown_files == null)
            new UnityException("Unable to initiate Texteditor.");

        // Setting up save button
        button_save.onClick.AddListener(() => SaveButtonPressed(text_editor.text));
        //text_editor.onEndEdit.AddListener((s) => SaveButtonPressed(s));
        
        text_editor.lineType = InputField.LineType.MultiLineNewline;

        filename = "ExampleNote";
        path = dir + filename + filetype;
        inputfield_filename.text = filename;

        if(!option_list.Contains(filename))
        {
            string text = "This is an example note file in Audinom.";
            text_editor.text = text;
            writer = new StreamWriter(path);
            writer.WriteLine(text);
            writer.Close();

            if(!option_list.Contains(filename))
            {
                option_list.Add(filename);
                dropdown_files.options.Add(new Dropdown.OptionData(filename));
            }
        } else
        {
            reader = new StreamReader(path);
            text_editor.text = reader.ReadToEnd();
            current_file = filename;
            reader.Close();
        }

        // subscribe to dropdown selection change event
        DropdownSelectionChangedScript.DropdownSelectionChanged += UpdateDisplayText;
    }

    void UpdateDisplayText(string name)
    {
        filename = name;
        path = dir + filename + filetype;
        reader = new StreamReader(path);
        string text = reader.ReadToEnd();
        text_editor.text = text;
        current_file = filename;
        inputfield_filename.text = filename;

        reader.Close();
    }

    public void ChangeFilename(string name)
    {
        filename = name;
        path = dir + filename + filetype;
    }

    public void SaveButtonPressed(string text)
    {
        try
        {
            writer = new StreamWriter(path);
        }
        catch(System.ArgumentNullException)
        {
            Debug.LogError("ArgumentNullException: Path argument = '" + path + "'");
        }

        writer.WriteLine(text);
        writer.Close();

        if(!option_list.Contains(filename))
        {
            option_list.Add(filename);
            dropdown_files.options.Add(new Dropdown.OptionData(filename));
        }

        // Save to filename list
        SaveState(option_list);

        current_file = filename;
    }

    public void DeleteCurrentNote()
    {
        try
        {
            File.Delete(path);
            option_list.Remove(filename);
            dropdown_files.options.RemoveAll(x => x.text.Equals(filename));
            SaveState(option_list);
            text_editor.text = "";
        }
        catch(Exception e)
        {
            Debug.LogError("[Notes] Could not delete file: " + path + "\n" + e.Message);
        }
    }

    private void SaveState(System.Collections.Generic.List<string> list)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/noteinfo.dat", FileMode.OpenOrCreate);

        NoteData data = new NoteData(list);

        bf.Serialize(file, data);
        file.Close();
    }

    private void LoadState()
    {
        if(File.Exists(Application.persistentDataPath + "/noteinfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenRead(Application.persistentDataPath + "/noteinfo.dat");
            NoteData data = (NoteData) bf.Deserialize(file);
            file.Close();

            foreach(string s in data.saved_notes)
            {
                if(File.Exists(dir + s + filetype))
                    option_list.Add(string.Copy(s));
            }
        }
    }
}

[Serializable]
class NoteData
{
    public System.Collections.Generic.List<string> saved_notes;

    public NoteData(System.Collections.Generic.List<string> data)
    {
        saved_notes = new System.Collections.Generic.List<string>();

        foreach(string s in data)
        {
            saved_notes.Add(string.Copy(s));
        }
    }
}
