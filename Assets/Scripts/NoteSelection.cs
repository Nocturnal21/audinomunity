﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class NoteSelection : MonoBehaviour
{
    public enum RandomType {Permutation = 0, OctaveFolding}
    private static NoteSelection instance;

    private const int NumberOfOctaves = 3;
    private List<uint> activeOctaves;
    public List<uint> Octaves { get { return activeOctaves; } }
    private List<uint> notes;
    public List<uint> Notes { get { return notes; } }

    public Playlist P;

    private static NoteSelection noteSelection;
    public static NoteSelection Instance
    {
        get
        {
            if (!noteSelection)
            {
                noteSelection = FindObjectOfType(typeof(NoteSelection)) as NoteSelection;
                if (!noteSelection)
                    Debug.LogError("There needs to be at least one active noteSelection on a GameObject in your scene!");
                else
                    noteSelection.Init();
            }
            return noteSelection;
        }
    }

    void Init()
    {
        notes = new List<uint>();
        activeOctaves = new List<uint>();
    }

    public void Add(uint note)
    {
        var octave = note >> 4;
        if (!activeOctaves.Contains(octave))
        {
            activeOctaves.Add(octave);
            notes.Add(note);
        }
        else if (!notes.Contains(note))
            notes.Add(note);
    }
    public void Remove(uint note)
    {
        var octave = note >> 4;
        notes.Remove(note);

        if (!notes.Exists(x => x >> 4 == octave))
            activeOctaves.Remove(octave);
    }

    public bool isEmpty()
    {
        return (notes.Count == 0);
    }

    public PlaylistEnumerator GetNewPlaylist(RandomType type)
    {
        P.RefreshPlaylist(type);
        return P.GetEnumerator();
    }

    public override string ToString()
    {
        string output = "[+] NOTE SELECTION" + Environment.NewLine;
        output += "Active octaves: ";
        foreach (var value in activeOctaves)
            output += string.Format("{0:X2} ", value);
        output += Environment.NewLine + "Selected notes: ";
        foreach (var note in notes)
            output += string.Format("{0:X2} ", note);
        output += Environment.NewLine;

        return output;
    }
}
