﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{

    public enum UIState { MainPanelBarsHidden = 0, MainPanelBarsVisible, RegisterPanel, SidePanel, PopUpPanel }
    public enum UISubState { Default, Register1, Register2, Register3, Register4 }

    private UIState currentState;
    public UIState CurrentState { get { return currentState; } }
    private UISubState currentSubState;
    public UISubState CurrentSubState { get { return currentSubState; } }

    // properties set by Unity Editor
    public MainViewController MainView;
    public RegisterViewController RegisterView;
    public SideViewController SideView;

    private static StateManager instance;
    public static StateManager Instance { get { return instance; } }

    // SideView
    public Animator AnimatorSidePanel;
    private string sidePanelHideTrigger = "CompleteHiddenTrigger";
    private int spTrigger;

    // Use this for initialization
    void Start()
    {
        //Show MainPanel
        currentState = UIState.MainPanelBarsHidden;
        currentSubState = UISubState.Default;
        StartCoroutine(MainPanelBarsHiddenState());
        instance = this;
    }

    //------------- Game States ---------------//
    IEnumerator MainPanelBarsHiddenState()
    {
        // show main panel
        EventManager.TriggerEvent("HideBars", new Message(true));

        while (currentState == UIState.MainPanelBarsHidden)
        {
            yield return null;
        }

        // hide main panel or close application
    }

    IEnumerator MainPanelBarsVisibleState()
    {
        EventManager.TriggerEvent("HideBars", new Message(false));

        while (currentState == UIState.MainPanelBarsVisible)
        {
            yield return null;
        }
    }

    IEnumerator RegisterPanelState()
    {
        if (!SideView.IsHidden)
            SideView.Hide();

        spTrigger = Animator.StringToHash(sidePanelHideTrigger);
        AnimatorSidePanel.SetBool(spTrigger, true);

        RegisterView.Show();
        currentSubState = UISubState.Register1;

        while (currentState == UIState.RegisterPanel)
        {
            yield return null;
        }

        currentSubState = UISubState.Default;
        RegisterView.Hide();

        AnimatorSidePanel.SetBool(spTrigger, false);
    }

    IEnumerator SidePanelState()
    {
        if (!RegisterView.IsHidden)
            RegisterView.Hide();

        SideView.Show();

        while (currentState == UIState.SidePanel)
        {
            yield return null;
        }

        SideView.Hide();
    }

    public void ChangeState(UIState newState)
    {
        currentState = newState;
        StartCoroutine(newState.ToString() + "State");
        Debug.Log("[State] " + newState.ToString());
    }

    public void ChangeSubState(UISubState newSubState)
    {
        currentSubState = newSubState;
    }
}
