﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelDotsScript : MonoBehaviour {

    public DotScript[] Dots; 

    public void ToggleDot(int octave, int note)
    {
        int index = (octave - 1) * 12 + (note - 1);
        Dots[index].Toggle();
    }
}
