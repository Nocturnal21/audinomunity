﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctaveWindowScript : MonoBehaviour {
    public delegate void OctaveDrag(float x);
    public static event OctaveDrag OctaveDragStart;

    public void StartDrag()
    {
        if(Input.touchCount > 0)
        {
            Touch t = Input.touches[0];

            OctaveDragStart(t.position.x);
        }
    }
}
