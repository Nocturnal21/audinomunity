﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DotScript : MonoBehaviour {
    public Sprite NotSelectedImage, SelectedImage;

    private Image image;
    private bool selected;

	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        selected = false;
    }

    public void Toggle()
    {
        if (selected)
            image.sprite = NotSelectedImage;
        else
            image.sprite = SelectedImage;

        selected = !selected;
    }
}
