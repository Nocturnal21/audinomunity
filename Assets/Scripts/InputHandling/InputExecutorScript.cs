﻿using UnityEngine;

public class InputExecutorScript : MonoBehaviour
{

    //Panels
    public GameObject MainPanel;
    public GameObject RegisterPanel;
    public GameObject SidePanel;

    //SubPanels
    public GameObject SelectionPanel;

    private StateManager stateManager;

    //Helper variables
    public RectTransform sidePanelHandle;
    public RectTransform mainPanelButton;

    //RegisterPanel
    public RegisterPanelScript regScript;
    public RectTransform [] regs;

    // Use this for initialization
    void Start()
    {
        stateManager = StateManager.Instance;
    }

    public void ProcessInput(InputInterpreterScript.Gestures gesture, Vector2 touchStart = new Vector2(), InputInterpreterScript.StrokeLength strokeLength = InputInterpreterScript.StrokeLength.Tap)
    {
        switch (stateManager.CurrentState)
        {
            case StateManager.UIState.MainPanelBarsHidden:        // IN MAINPANEL - BARS HIDDEN ----------------------
                if (gesture == InputInterpreterScript.Gestures.Tap &&
                !RectTransformUtility.RectangleContainsScreenPoint(mainPanelButton, touchStart))
                    stateManager.ChangeState(StateManager.UIState.MainPanelBarsVisible);
                break;
            case StateManager.UIState.MainPanelBarsVisible:         // IN MAINPANEL - BARS VISIBLE ---------------------
                if (gesture == InputInterpreterScript.Gestures.Tap &&
                    !RectTransformUtility.RectangleContainsScreenPoint(mainPanelButton, touchStart))
                    stateManager.ChangeState(StateManager.UIState.MainPanelBarsHidden);
                else if (gesture == InputInterpreterScript.Gestures.SwipeBtoT &&
                    strokeLength > InputInterpreterScript.StrokeLength.ShortD &&
                    InputInterpreterScript.Instance.RectBottom.Contains(touchStart))
                {
                    stateManager.ChangeState(StateManager.UIState.RegisterPanel);
                    //check which panel dragged
                   for(var i = 0; i < regs.Length; ++i) {
                      if(RectTransformUtility.RectangleContainsScreenPoint(regs[i], touchStart))
                        {
                            regScript.BringRegisterToFront(i + 1); //Register IDs start with 1
                            continue;
                        }
                    }
                }
                else if (gesture == InputInterpreterScript.Gestures.SwipeLtoR &&
                    strokeLength > InputInterpreterScript.StrokeLength.ShortD)
                    if (RectTransformUtility.RectangleContainsScreenPoint(sidePanelHandle, touchStart))
                        stateManager.ChangeState(StateManager.UIState.SidePanel);
                break;
            case StateManager.UIState.RegisterPanel:    // IN REGISTERPANEL ------------------
                if (gesture == InputInterpreterScript.Gestures.SwipeTtoB &&
                    strokeLength > InputInterpreterScript.StrokeLength.ShortD &&
                    InputInterpreterScript.Instance.RectTop.Contains(touchStart))
                    stateManager.ChangeState(StateManager.UIState.MainPanelBarsVisible);
                else if (gesture == InputInterpreterScript.Gestures.Tap)
                {
                    RectTransform r = RegisterPanel.GetComponent<RectTransform>();
                    if (RectTransformUtility.RectangleContainsScreenPoint(r, touchStart) == false)
                        stateManager.ChangeState(StateManager.UIState.MainPanelBarsVisible);
                }
                else if (stateManager.CurrentSubState == StateManager.UISubState.Register1)
                {
                    RectTransform r = SelectionPanel.GetComponent<RectTransform>();
                    if (strokeLength > InputInterpreterScript.StrokeLength.ShortD &&
                        RectTransformUtility.RectangleContainsScreenPoint(r, touchStart))
                    {
                        if (gesture == InputInterpreterScript.Gestures.SwipeLtoR)
                        {
                            EventManager.TriggerEvent("DecreaseOctave", null);
                        }
                        else if (gesture == InputInterpreterScript.Gestures.SwipeRtoL)
                        {
                            EventManager.TriggerEvent("IncreaseOctave", null);
                        }
                    }
                }
                break;
            case StateManager.UIState.SidePanel:        // IN SIDEPANEL -----------------------
                if (gesture == InputInterpreterScript.Gestures.SwipeRtoL &&
                    InputInterpreterScript.Instance.RectRight.Contains(touchStart))
                    stateManager.ChangeState(StateManager.UIState.MainPanelBarsVisible);
                break;
        }
    }
}
