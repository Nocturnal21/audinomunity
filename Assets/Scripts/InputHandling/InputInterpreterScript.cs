﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputInterpreterScript : MonoBehaviour
{

    public enum Gestures { Tap, SwipeLtoR, SwipeRtoL, SwipeBtoT, SwipeTtoB }
    public enum StrokeLength { Tap, ShortD, MidD, LongD }
    private enum Axis { X, Y }

    public InputExecutorScript Executor;

    private Vector2 touchStart = -Vector2.one;
    private Vector2 touchDelta = Vector2.zero;
    private bool ignore = false;

    private const float edgeBT = 0.15f;
    private const float edgeLR = 0.22f;

    private const float shortDistance = 0.1f;
    private const float midDistance = 0.3f;
    private const float longDistance = 0.6f;
    private const int tapThreshold = 15;

    public Rect RectLeft, RectRight, RectBottom, RectTop;

    private static InputInterpreterScript instance;
    public static InputInterpreterScript Instance { get { return instance; } }

    // Use this for initialization
    void Start()
    {
        instance = this;

        RectLeft = new Rect(0, 0, Screen.width * edgeLR, Screen.height);
        RectRight = new Rect(Screen.width * (1 - edgeLR), 0, Screen.width * edgeLR, Screen.height);
        RectBottom = new Rect(0, 0, Screen.width, Screen.height * edgeBT);
        RectTop = new Rect(0, Screen.height * (1 - edgeBT), Screen.width, Screen.height * edgeBT);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)                            // TOUCH BEGIN
            {
                touchStart = touch.position;
            }
            else if(ignore == false && touch.phase == TouchPhase.Moved && touchStart.x > 0) // TOUCH CHANGED
            {
                touchDelta += touch.deltaPosition;

                if(touchDelta.magnitude > Screen.height * shortDistance)
                {
                    if (Mathf.Abs(touchDelta.x) > Mathf.Abs(touchDelta.y))     // Horizontal Swipe
                    {
                        if (touchDelta.x > 0)               // Left to Right
                        {
                            Executor.ProcessInput(Gestures.SwipeLtoR, touchStart, GetStrokeLength(touchDelta.magnitude, Axis.X));
                        }
                        else                  // Right to Left
                        {
                            Executor.ProcessInput(Gestures.SwipeRtoL, touchStart, GetStrokeLength(touchDelta.magnitude, Axis.X));
                        }
                    }
                    else                              // Vertical Swipe
                    {
                        if (touchDelta.y > 0)               // Bottom to Top
                        {
                            Executor.ProcessInput(Gestures.SwipeBtoT, touchStart, GetStrokeLength(touchDelta.magnitude, Axis.Y));
                        }
                        else                  // Top to Bottom
                        {
                            Executor.ProcessInput(Gestures.SwipeTtoB, touchStart, GetStrokeLength(touchDelta.magnitude, Axis.Y));
                        }
                    }
                    ignore = true;
                }
            }
            else if (touch.phase == TouchPhase.Ended && touchStart.x > 0)   // TOUCH END
            {
                if(ignore == false)
                {
                    Vector2 touchEnd = touch.position;
                    //check direction
                    Vector2 v = touchEnd - touchStart;
                    //check magnitude
                    if (v.magnitude < tapThreshold)          // --- TAP ---
                        Executor.ProcessInput(Gestures.Tap, new Vector2(touchStart.x, touchStart.y), StrokeLength.Tap);
                }

                // RESET
                touchStart.x = -1;
                touchDelta = Vector2.zero;
                ignore = false;
            }
        }
    }

    private StrokeLength GetStrokeLength(float magnitude, Axis axis)
    {
        float distance;
        if (axis == Axis.X)
            distance = Screen.width;
        else
            distance = Screen.height;

        if (magnitude < distance * shortDistance)
            return StrokeLength.ShortD;
        else if (magnitude < distance * midDistance)
            return StrokeLength.MidD;
        else
            return StrokeLength.LongD;
    }
}
