﻿using System.Collections.Generic;
using UnityEngine;

public class SoundSettingScript : MonoBehaviour
{

    private AudioClip[] partSoundSet; //3 sounds per octave - cis, fis, a
    public AudioClip[] SoundSetPitched { get { return partSoundSet; } }
    private float[] pitchFactors;
    public float[] PitchFactors { get { return pitchFactors; } }
    private float pitchFactorReferenceA;
    private readonly System.Collections.Generic.List<string> SoundPackIds = new System.Collections.Generic.List<string>();

    private static readonly float defaultHertzA = 440.0f;
    private static readonly float[] pitchBasis = { 0.943877f, 1.0f, 1.05946f, 1.122455f };

    public AudioClip[] VoiceSoundsList;
    private Dictionary<string, AudioClip> voiceSounds;
    private string[][] voiceLabels;
    private int voiceLabelSelection;

    // Use this for initialization
    void Start()
    {
        voiceLabels = new string[3][];
        voiceLabels[0] = new string[] { "C", "Cis", "D", "Es", "E", "F", "Fis", "G", "As", "A", "B", "H" };
        voiceLabels[1] = new string[] { "C", "Cis", "D", "Dis", "E", "F", "Fis", "G", "Gis", "A", "Ais", "H" };
        voiceLabels[2] = new string[] { "C", "Des", "D", "Es", "E", "F", "Ges", "G", "As", "A", "B", "H" };
        voiceLabelSelection = 0;

        voiceSounds = new Dictionary<string, AudioClip>();
        foreach (var voice in VoiceSoundsList)
            voiceSounds.Add(voice.name, voice);

        SoundPackIds.Add("Piano");
        SoundPackIds.Add("Organ");
        SoundPackIds.Add("Sine");
        SoundPackIds.Add("Rhodes");

        //default
        LoadPitchedSoundpack(0);
    }

    private void LoadPitchedSoundpack(int soundpackId) {
        if(soundpackId >= 0 && soundpackId < SoundPackIds.Count) {
            pitchFactors = new float[12];
            partSoundSet = ResourceLoader.LoadSoundPackFragment(SoundPackIds[soundpackId], "C1", "C2", "C3");
            if (partSoundSet == null)
                Debug.LogError("Could not load Soundpack: " + soundpackId);
            else
            {
                //1.) Set reference pitch for 'a'
                pitchFactorReferenceA = defaultHertzA / defaultHertzA;
                //2.) Adjust all other tones
                GeneratePitchFactors();
                //3.) Raise event that soundpack has changed
                EventManager.TriggerEvent("SoundpackChanged", null);
            }
        }
        else
        {
            Debug.LogError("Could not load Soundpack: " + soundpackId);
        }
    }

    private void GeneratePitchFactors() {
        for(var i = 0; i < 3; ++i)
        {
            pitchFactors[i*4] = pitchFactorReferenceA * pitchBasis[0];
            pitchFactors[i*4 + 1] = pitchFactorReferenceA; //no need to mult with pitchBasis[1] -> 1.0
            pitchFactors[i*4 + 2] = pitchFactorReferenceA * pitchBasis[2];
            pitchFactors[i*4 + 3] = pitchFactorReferenceA * pitchBasis[3];
        }
    }


    public void SoundPackSelectionChanged(int id)
    {
        LoadPitchedSoundpack(id);
    }

    public void VoiceLabellingChanged(int id)
    {
        if (id >= voiceLabels.Length) //no voice
        {
            EventManager.TriggerEvent("VoiceEnabled", new Message(false));
        }
        else
        {
            EventManager.TriggerEvent("VoiceEnabled", new Message(true));
        }
        voiceLabelSelection = id;
    }

    /// <summary>
    /// Getting current voice file. (1-based indexing)
    /// </summary>
    /// <param name="index">note index (1-based) </param>
    /// <returns>Audioclip corresponding to note index</returns>
    public AudioClip GetVoice(int index)
    {
            return voiceSounds[voiceLabels[voiceLabelSelection][index - 1]];
    }
}
