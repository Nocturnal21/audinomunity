﻿using UnityEngine;
using System.Collections;

public class MetronomeScript : MonoBehaviour {
    public const int PreCount = 4;
    public AudioClip[] sounds;

    public PlayTickScript normalTick;
    public PlayTickScript emphTick;

    private AudioClip normal_sound;
    private AudioClip emph_sound;

    private int tick_counter;

    // Use this for initialization
    void Start () {
        normal_sound = sounds[0];
        emph_sound = sounds[1];

        normalTick.SetClip(normal_sound);
        emphTick.SetClip(emph_sound);

        EventManager.Instance.StartListening("Stop", Reset);
        Reset(null);
    }

    public void Tick()
    {
        if (tick_counter % AudioPlayerScript.BeatsPerNote == 0)
            emphTick.Play();
        else
            normalTick.Play();

        ++tick_counter;
    }

    public void Reset(Message m)
    {
        tick_counter = 0;
    }

    public void ChangeNormalSound(int value)
    {
        normal_sound = sounds[value];
    }

    public void ChangeEmphSound(int value)
    {
        emph_sound = sounds[(value + 1) % 3];
    }
}
