﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class AudioMixerControlScript : MonoBehaviour {
    public AudioMixer mixer;

    void Start()
    {
        mixer.SetFloat("volume_notes", -10.0f);
    }
    
    public void SetMasterVolume(float value)
    {
        mixer.SetFloat("volume_master", value);
    }

    public void SetSoundVolume(float value)
    {
        mixer.SetFloat("volume_notes", value);
    }

    public void SetMetronomeVolume(float value)
    {
        mixer.SetFloat("volume_metronome", value);
    }
}
