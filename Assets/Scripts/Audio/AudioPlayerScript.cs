﻿using UnityEngine;
using System.Diagnostics;
using System;

public class AudioPlayerScript : MonoBehaviour
{
    private AudioClip[] sounds;
    private float[] pitchFactors;

    private AudioSource SoundAudioSource;

    public MetronomeScript Metronome;
    public SoundSettingScript SoundSettings;

    private bool running;
    private Stopwatch timer;
    private long interval; // Time interval between beats
    private int bpm;
    private long previousStoppedTime;

    private static int bpn = 4;
    public static int BeatsPerNote { get { return bpn; } }
    private int soundDuration;
    private int tick_counter;
    private int duration_counter;
    private int pre_count = MetronomeScript.PreCount;
    private NoteSelection.RandomType CurrentPlaylistRandomizer;

    private UnityEngine.UI.Text display_note;

    private NoteSelection Selection;
    private PlaylistEnumerator playlistEnumerator;

    private delegate void Player();
    Player AudioPlayer;

    // PRESET STUFF
    private bool isPresetModeOn = false;
    private Preset currentPreset;
    public AudioSource[] AudioSources;
    public AudioSource VoiceAudioSource;
    int accordIndex = 0;
    uint cadenceStartNote = 0;

    //VOICE STUFF
    private bool isVoiceEnabled;

    // Use this for initialization
    void Awake()
    {
        UINoteSelection.RefreshSelection += RefreshSelection;

        display_note = GameObject.Find("DisplayNote").GetComponent<UnityEngine.UI.Text>();

        SoundAudioSource = GetComponent<AudioSource>();
        isVoiceEnabled = false;

        Selection = NoteSelection.Instance;
        CurrentPlaylistRandomizer = NoteSelection.RandomType.Permutation;
        tick_counter = duration_counter = 0;
        soundDuration = bpn;

        bpm = 90;
        interval = (long)Mathf.Round(60000.0f / bpm);

        timer = new Stopwatch();
        previousStoppedTime = 0;
        running = false;

        //subscribe to event handlers in UI
        EventManager.Instance.StartListening("SoundpackChanged", ChangeSoundpack);
        EventManager.Instance.StartListening("BPNSlider", ChangeBPN);   // Beats per Note changed
        EventManager.Instance.StartListening("BPMSlider", ChangeBPM);    // Beats per Minute changed
        EventManager.Instance.StartListening("DurationSlider", ChangeSoundDuration);    // Sound Duration Changed
        EventManager.Instance.StartListening("EnablePresetMode", ChangePlayerMode);
        EventManager.Instance.StartListening("VoiceEnabled", ChangeVoiceEnabled);
        EventManager.Instance.StartListening(PresetHandler.presetChangedEventName, PresetChanged);

        AudioPlayer = DefaultPlayer;
    }

    public void ChangeVoiceEnabled(Message arg0)
    {
        isVoiceEnabled = arg0.BoolValue;
    }

    private void OnDestroy()
    {
        EventManager.StopListening("DurationSlider", ChangeSoundDuration); // unsubscribe
        EventManager.StopListening("BPNSlider", ChangeBPN); // unsubscribe
        EventManager.StopListening("BPMSlider", ChangeBPN); // unsubscribe
        EventManager.StopListening("SoundPackChanged", ChangeSoundpack);
        EventManager.StopListening("EnablePresetMode", ChangePlayerMode);
        EventManager.StopListening("VoiceEnabled", ChangeVoiceEnabled);
        EventManager.StopListening(PresetHandler.presetChangedEventName, PresetChanged);
    }

    void Update()
    {
        AudioPlayer();
    }

    private void DefaultPlayer()
    {
        if (running && (timer.ElapsedMilliseconds - previousStoppedTime) >= interval)
        {
            previousStoppedTime = timer.ElapsedMilliseconds;

            Metronome.Tick();

            if (pre_count == 0)
            {
                tick_counter++;

                if (tick_counter % bpn == 1)
                {

                    SoundAudioSource.Stop();
                    VoiceAudioSource.Stop();
                    if (playlistEnumerator != null && playlistEnumerator.MoveNext())
                    {
                        uint currentNote = playlistEnumerator.Current();
                        duration_counter = 0;

                        PlayNote(currentNote);

                        if(isVoiceEnabled)
                        {
                            PlayVoice(currentNote);
                        }
                        ChangeDisplayText(currentNote);
                    }
                }

                if (duration_counter == soundDuration)
                {
                    SoundAudioSource.Stop();
                    VoiceAudioSource.Stop();
                }
                ++duration_counter;
            }
            else
            {
                --pre_count;
            }
        }
    }

    private void PresetPlayer()
    {
        if(running && (timer.ElapsedMilliseconds - previousStoppedTime) >= interval)
        {
            previousStoppedTime = timer.ElapsedMilliseconds;

            Metronome.Tick();

            if (pre_count == 0)
            {
                tick_counter++;

                if (tick_counter % bpn == 1)
                {

                    foreach (var a in AudioSources)
                        a.Stop();

                    if(playlistEnumerator != null && playlistEnumerator.MoveNext() && accordIndex == 0)
                    {
                        cadenceStartNote = playlistEnumerator.Current();
                        duration_counter = 0;

                        // PLAY CURRENT PRESET ACCORD
                        PlayAccord(currentPreset.Accords[accordIndex], cadenceStartNote);
                        ChangeDisplayText(cadenceStartNote);
                        accordIndex = ++accordIndex % currentPreset.Beats;
                    }
                    else if(accordIndex > 0)
                    {
                        duration_counter = 0;
                        PlayAccord(currentPreset.Accords[accordIndex], cadenceStartNote);
                        ChangeDisplayText(cadenceStartNote);
                        accordIndex = ++accordIndex % currentPreset.Beats;
                    }
                }

                if (duration_counter == soundDuration)
                {
                    foreach (var a in AudioSources)
                        a.Stop();
                }
                ++duration_counter;
            }
            else
            {
                --pre_count;
            }
        }
    }

    /// <summary>
    /// Decodes note id, calculates pitch and plays sound [CHECK]
    /// </summary>
    private void PlayNote(uint id)
    {
        uint octave = id >> 4;

        AudioClip clip = null;
        float factor = 1.0f;
        if (octave >= 1 && octave <= 3)
        {
            uint note = (15u & id); // 1-based indexing
            var iBase = -1;
            int diff;

            if (note >= 1 && note <= 4)
            {
                iBase = 0;
                diff = (int)note - 2;
            }
            else if (note <= 8)
            {
                iBase = 1;
                diff = (int)note - 6;
            }
            else if (note <= 12)
            {
                iBase = 2;
                diff = (int)note - 10;
            }
            else
            {
                throw new System.IndexOutOfRangeException("[AudioPlayerScript] noteid not found o/n: " + octave + " " + note);
            }

            clip = sounds[iBase + (octave - 1) * 3];
            factor = pitchFactors[diff + 1];
        }

        SoundAudioSource.pitch = factor;
        SoundAudioSource.PlayOneShot(clip);
    }

    private void PlayVoice(uint id)
    {
        uint note = (15u & id);
        VoiceAudioSource.PlayOneShot(SoundSettings.GetVoice((int)note));
    }

    private void PlayNote(uint id, int audioSourceIndex)
    {
        uint octave = id >> 4;

        var source = AudioSources[audioSourceIndex];
        AudioClip clip = null;
        float factor = 1.0f;
        if (octave >= 1 && octave <= 3)
        {
            uint note = (15u & id); // 1-based indexing
            var iBase = -1;
            int diff;

            if (note >= 1 && note <= 4)
            {
                iBase = 0;
                diff = (int)note - 2;
            }
            else if (note <= 8)
            {
                iBase = 1;
                diff = (int)note - 6;
            }
            else if (note <= 12)
            {
                iBase = 2;
                diff = (int)note - 10;
            }
            else
            {
                throw new System.IndexOutOfRangeException("[AudioPlayerScript] noteid not found o/n: " + octave + " " + note);
            }

            clip = sounds[iBase + (octave - 1) * 3];
            factor = pitchFactors[diff + 1];
        }

        source.pitch = factor;
        source.clip = clip;
        source.Play();
    }

    private uint CalcNote(int basisnote, int basisoctave, int interval)
    {
        int octave = basisoctave;
        int note = basisnote + interval;
        if (note == 0)
        {
            note = 12;
            octave -= 1;
        }
        else if (note > 12)
        {
            if(note % 12 == 0)
            {
                while(note > 12)
                {
                    note -= 12;
                    octave += 1;
                }
            } else
            {
                note %= 12;
                octave += 1;
            }
        }
        else if (note < 0)
        {
            var r = note % 12;
            note = r < 0 ? r + 12 : r;
            octave -= 1;
        }

        return (uint)((octave << 4) + note);
    }

    private void PlayAccord(Accord accord, uint cadenceStart)
    {
        uint[] notes = new uint[accord.Cardinality];
        int startNote = (int)(cadenceStart & 15);
        int startOctave = (int)(cadenceStart >> 4);

        uint basis = CalcNote(startNote,startOctave,accord.Basis);
        int basisnote = (int)(basis & 15);
        int basisoctave = (int)(basis >> 4);
        string accordString = "[ ";

        for(int i = 0; i < notes.Length; i++)
        {
            int interval = accord.Relatives[i];
            notes[i] = CalcNote(basisnote, basisoctave, interval);

            accordString += "" + (notes[i] >> 4) + "/" + (notes[i] & 15) + " , ";
        }

        UnityEngine.Debug.Log("Accord: " + accordString);

        for(int i = 0; i < accord.Cardinality; ++i)
        {
            PlayNote(notes[i], i);
        }
    }

    void Stop()
    {
        EventManager.TriggerEvent("Stop", null);
        running = false;
        tick_counter = 0;
        pre_count = MetronomeScript.PreCount;

        if (isPresetModeOn)
        {
            SoundAudioSource.Stop();
        }
        else
        {
            foreach (var a in AudioSources)
                a.Stop();
            accordIndex = 0;
        }

        //timer reset
        timer.Reset();
        previousStoppedTime = 0;
    }

    /// <summary>
    /// Called when new notes are selected by user
    /// </summary>
    public void RefreshSelection()
    {
        // do sth like refreshing playlist
        playlistEnumerator = Selection.GetNewPlaylist(CurrentPlaylistRandomizer);
    }

    public void OnRandomizerChanged(int value)
    {
        CurrentPlaylistRandomizer = (NoteSelection.RandomType)value;
        RefreshSelection();
    }

    private void ChangeDisplayText(uint note)
    {
        switch (note & 15u)
        {
            case 1:
                display_note.text = "C";
                break;
            case 2:
                display_note.text = "Cis";
                break;
            case 3:
                display_note.text = "D";
                break;
            case 4:
                display_note.text = "Es";
                break;
            case 5:
                display_note.text = "E";
                break;
            case 6:
                display_note.text = "F";
                break;
            case 7:
                display_note.text = "Fis";
                break;
            case 8:
                display_note.text = "G";
                break;
            case 9:
                display_note.text = "As";
                break;
            case 10:
                display_note.text = "A";
                break;
            case 11:
                display_note.text = "B";
                break;
            case 12:
                display_note.text = "H";
                break;
            default:
                throw new System.IndexOutOfRangeException("[NoteDisplay] Note not found: " + note);
        }
    }


    public void ChangeSoundpack(Message m)
    {
        Stop();
        sounds = SoundSettings.SoundSetPitched;
        pitchFactors = SoundSettings.PitchFactors;

        if (!NoteSelection.Instance.isEmpty())
            PlayPause();
    }

    public static void ChangeBPN(Message m)
    {
        var value = m.IntValue;
        if (value > 0)
            bpn = value;
    }

    public void ChangeSoundDuration(Message m)
    {
        var value = m.IntValue;
        if (value > bpn)
            soundDuration = bpn;
        else
            soundDuration = value;
    }

    public void ChangeBPM(Message m)
    {
        this.bpm = m.IntValue;
        this.interval = (long)Mathf.Round(60000.0f / this.bpm);

        //Stop();
    }

    public void PlayPause()
    {
        if (!running)
        {
            timer.Start();
            running = true;
        }
        else
        {
            running = false;
            Stop();
        }
    }

    public void ChangePlayerMode(Message m)
    {
        Stop();

        UnityEngine.Debug.Log("[AudioPlayerScript] PresetMode: " + m.BoolValue);
        isPresetModeOn = m.BoolValue;

        if (isPresetModeOn)
            AudioPlayer = PresetPlayer;
        else
            AudioPlayer = DefaultPlayer;
    }

    public void PresetChanged(Message m)
    {
        accordIndex = 0;
        currentPreset = PresetLoader.Instance.LoadedPresets[m.StringValue];
    }
}
