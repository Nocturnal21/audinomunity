﻿using UnityEngine;

public class PlayTickScript : MonoBehaviour {

    AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
	
    public void Play()
    {
        source.Play();
    }

    public void SetClip(AudioClip clip)
    {
        source.clip = clip;
    }
}
