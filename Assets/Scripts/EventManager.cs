﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MessageEvent : UnityEvent<Message> { }

public class Message
{
    private int intValue;
    public int IntValue { get { return intValue; } }

    private string stringValue;
    public string StringValue { get { return stringValue; } }

    private bool boolValue;
    public bool BoolValue { get { return boolValue; } }

    private ValueType type;
    public ValueType messageType { get { return type; } }

    public enum ValueType { Integer, String, Boolean};

    public Message(int i) { intValue = i; type = ValueType.Integer; }
    public Message(string s) { stringValue = s; type = ValueType.String; }
    public Message(bool b) { boolValue = b;  type = ValueType.Boolean; }
}

public class EventManager : MonoBehaviour {

    private Dictionary<string, MessageEvent> eventDictionary;
    private static EventManager eventManager;
    public static EventManager Instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
                if (!eventManager)
                    Debug.LogError("There needs to be at least one active EventManager on a GameObject in your scene!");
                else
                    eventManager.Init();
            }
            return eventManager;
        }
    }

    void Init()
    {
        if(eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, MessageEvent>();
        }
    }

    public void StartListening(string eventName, UnityAction<Message> listener)
    {
        MessageEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        } else
        {
            thisEvent = new MessageEvent();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction<Message> listener)
    {
        if (eventManager == null) return;
        MessageEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, Message message)
    {
        MessageEvent thisEvent = null;
        if(Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(message);
        }
    }
}
