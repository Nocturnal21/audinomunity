﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;

public class PresetLoader : MonoBehaviour {
    public List<TextAsset> presetNames;

    private static PresetLoader instance;
    public static PresetLoader Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType(typeof(PresetLoader)) as PresetLoader;
                if (!instance)
                    Debug.LogError("There needs to be at least one active PresetLoader on a GameObject in your scene!");
            }
            return instance;
        }
    }

    public Dictionary<string, Preset> LoadedPresets
    {
        get
        {
            return loadedPresets;
        }
    }
    private Dictionary<string, Preset> loadedPresets;

	// Use this for initialization
	void Start () {
        EventManager.Instance.StartListening(PresetHandler.presetChangedEventName, PresetChanged);

        loadedPresets = new Dictionary<string, Preset>();
        //string wdir = System.IO.Directory.GetCurrentDirectory();
        loadPresets();
	}

    private void OnDestroy()
    {
        EventManager.StopListening("PresetChanged", PresetChanged);
    }

    private void loadPresets()
    {
        foreach(var item in presetNames)
        {
            XmlDocument presetXml = new XmlDocument();
            presetXml.LoadXml(item.text);
            XmlNode meta = presetXml.SelectSingleNode("preset/meta");
            XmlNode sounds = presetXml.SelectSingleNode("preset/sounds");

            int beats, cardinal;
            string name;
            List<string> enabled = new List<string>();

            // PARSING META DATA
            name = meta.ChildNodes[0].InnerText.Trim();

            if (!int.TryParse(meta.ChildNodes[1].InnerText, out beats))
            {
                Debug.LogError("Couldn't load Preset Detail: " + "<beats>");
            }
            if (!int.TryParse(meta.ChildNodes[2].InnerText, out cardinal))
            {
                Debug.LogError("Couldn't load Preset Detail: " + "<cardinal>");
            }
            var enabledNotes = meta.ChildNodes[3];
            foreach (XmlNode node in enabledNotes.ChildNodes)
            {
                enabled.Add(node.InnerText.Trim());
            }

            Preset preset = new Preset(name, enabled, cardinal);
            // PARSING INTERVAL DATA
            foreach (XmlNode node in sounds.ChildNodes)
            {
                var acc = new Accord(int.Parse(node.ChildNodes[0].InnerText));
                for (int i = 1; i < node.ChildNodes.Count; i++)
                {
                    acc.addRelative(int.Parse(node.ChildNodes[i].InnerText));
                }
                preset.AddAccord(acc);
            }

            loadedPresets.Add(name, preset);
        }

        EventManager.TriggerEvent("AllPresetsLoaded", null);
    }

    public void PresetChanged(Message message)
    {

    }
}
