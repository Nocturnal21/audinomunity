﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preset {
    private string name;
    private List<string> enabledBaseNotes;
    private List<Accord> accords;
    private int cardinality;

    public int Beats
    {
        get
        {
            return accords.Count;
        }
    }

    public List<string> EnabledBaseNotes
    {
        get
        {
            return enabledBaseNotes;
        }
    }
    public List<Accord> Accords
    {
        get
        {
            return accords;
        }
    }
    public int Cardinality
    {
        get
        {
            return cardinality;
        }
    }
    public string Name
    {
        get
        {
            return name;
        }
    }

    public Preset(string name, List<string> enabledBaseNotes, int cardinality)
    {
        this.name = name;
        this.enabledBaseNotes = enabledBaseNotes;
        this.cardinality = cardinality;
        accords = new List<Accord>();
    }

    public void AddAccord(Accord a)
    {
        accords.Add(a);
    }
}
