﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresetHandler : MonoBehaviour {
    public const string presetChangedEventName = "PresetChanged";

    public GameObject PresetEntryPrefab;

    private ToggleGroup presetToggles;
    private PresetLoader loader;
	// Use this for initialization
	void Start () {
        presetToggles = GetComponent<ToggleGroup>();
        EventManager.Instance.StartListening("AllPresetsLoaded", FillPanelWithEntries);
        loader = PresetLoader.Instance;
	}

    public void FillPanelWithEntries(Message m)
    {
        foreach(var key in loader.LoadedPresets.Keys)
        {
            var entry = Instantiate(PresetEntryPrefab, this.gameObject.transform, false);
            entry.GetComponent<Toggle>().group = presetToggles;
            var component = entry.GetComponent<CustomToggleScript>();
            component.eventName = presetChangedEventName;
            component.presetName = key;
        }        
    }
}
