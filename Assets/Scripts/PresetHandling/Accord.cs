﻿using System.Collections.Generic;

public class Accord {
    private int basis;
    private List<int> relatives;

    public int Cardinality
    {
        get
        {
            return relatives.Count;
        }
    }
    public List<int> Relatives
    {
        get
        {
            return relatives;
        }
    }

    public int Basis
    {
        get
        {
            return basis;
        }

        set
        {
            basis = value;
        }
    }

    public Accord(int basis)
    {
        relatives = new List<int>();
        this.Basis = basis;
    }

    public void addRelative(int rel)
    {
        relatives.Add(rel);
    }
}
