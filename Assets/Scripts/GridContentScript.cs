﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GridContentScript : MonoBehaviour {


    public int rows;
    public int cols;
    public GameObject[] ButtonList;

    void Start()
    {
        RectTransform parentRect = gameObject.GetComponent<RectTransform>();
        GridLayoutGroup gridLayout = gameObject.GetComponent<GridLayoutGroup>();
        gridLayout.cellSize = new Vector2(parentRect.rect.width / cols, parentRect.rect.height / rows);

        foreach(GameObject button in ButtonList)
        {
            button.transform.SetParent(gameObject.transform, false);
        }
    }
}
