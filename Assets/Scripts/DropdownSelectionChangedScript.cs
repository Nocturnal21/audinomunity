﻿using UnityEngine;
using System.Collections;

public class DropdownSelectionChangedScript : MonoBehaviour
{
    public delegate void SelectionHandler(string sel);
    public static event SelectionHandler DropdownSelectionChanged;

    private UnityEngine.UI.Dropdown dropdown;
    private System.Collections.Generic.List<UnityEngine.UI.Dropdown.OptionData> option_list;

    // Use this for initialization
    void Start()
    {
        dropdown = this.GetComponentInParent<UnityEngine.UI.Dropdown>();
        option_list = dropdown.options;
    }

    public void SelectionChanged(int value)
    {
        var item = option_list[value];
        string option_text = item.text;

        DropdownSelectionChanged(option_text);
    }
}
