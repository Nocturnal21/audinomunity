Project Setup:

Download
	* Unity Project
	* Sound Files
	
1.) Extract Project to your choosen destination
2.) Get the Sound files and copy them in the Unity Project directory
	into the folder 'Assets'. Make sure the sound files are in a folder
	named 'Resources'. (they should be in Resources as default)
	
Naming conventions and directory structure for new sounds:

	Directory Structure:
	
	NewSoundpackName ----> NewSoundpackName C1
					 |---> NewSoundpackName C2
					 \---> NewSoundpackName C3
					 
	Naming conventions:
	Copy the 36 sound files into each of the C1, C2, C3 folders.
	Naming: <note_index>_<octave_index>
	
	e.g.:	C in C1 -> 01_1
			Cis in C1 -> 02_1
			D in C1 -> 03_1
			...
			C in C2 -> 01_2
			...
			H in C3 -> 12_3

---------------------------------------------------------------
Android adb debug:

```
#!shell
adb logcat -s Unity ActivityManager PackageManager dalvikvm DEBUG

```

---------------------------------------------------------------
>>> Installing Your App on Test Devices Using Apple Configurator 2 (iOS, watchOS, tvOS) <<<

Apple Configurator 2 is a free app from the Mac App Store that makes it easy to install iOS and tvOS apps on connected test devices. 
For tvOS apps, this is the only way to install an iOS App file on an Apple TV without using Xcode.
To install the app on a device using Apple Configurator 2
Connect the testing device to a Mac running Apple Configurator 2.
If possible, don’t use a Mac that you use for development. For watchOS apps, connect an iPhone paired with an Apple Watch.
Select the device, click the Add button (+), and select Apps from the pop-up menu.
In the dialog that appears, click “Choose from my Mac”.
Choose the iOS App file that you created earlier and click Add.
---------------------------------------------------------------